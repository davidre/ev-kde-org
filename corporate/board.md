---
title: People
layout: page
menu_active: Organization
board:
  - name: Aleix Pol i Gonzàlez
    title: President
    email: aleixpol<span>@</span>kde.org
    description: Aleix Pol i Gonz&agrave;lez has been collaborating with KDE since 2007. He started working in
      software development in the KDE Education area and KDevelop.
      Aleix joined the KDE e.V. board of directors in 2014.
      In his day-job, he is employed by Blue Systems where he has worked with other parts of the community
      including Plasma and Qt.
    image: /corporate/pictures/apol.jpg
    elected: 2020
  - name: Eike Hein
    title: Treasurer and Vice President
    email: hein<span>@</span>kde.org
    description: Eike Hein has been a KDE contributor since 2005. Initially working
      on applications and later on Plasma as developer and designer, he has also served
      on KDE's Sysadmin team and co-authored the <a href="https://manifesto.kde.org/">KDE Manifesto</a>.
      Eike joined the KDE e.V. board of directors in 2017.
    image: /corporate/pictures/eike.jpg
    elected: 2020
  - name: Lydia Pintscher
    title: Vice President
    email: lydia<span>@</span>kde.org
    description: Lydia Pintscher has been with KDE since 2006. She started doing
      marketing, and later community and release management for Amarok. She
      has since moved on to community management and running mentoring
      programs for all of KDE. She is on the board of directors of KDE e.V.
      since 2011. In her day-job she does product management for
      Wikidata at Wikimedia Germany.
    image: /corporate/pictures/lydia.jpg
    elected: 2020
  - name: Neofytos Kolokotronis
    title: Board Member
    email: neofytosk<span>@</span>kde.org
    description: Neofytos joined KDE in 2017 via the Promo team, however when the KDE goal he proposed on "Streamlined Onboarding of new contributors" was voted by the community, he had to turn his full attention to leading it and pushing for achieving its objectives. He joined KDE e.V. in 2018, became a member of the e.V.'s Financial Working Group some months later and was elected in the Board of Directors in 2019. He has been a founder and contributor to FOSS and Open Data/Government projects on an international and local level for more than a decade, serving from a variety of positions.
    image: /corporate/pictures/neofytos.jpg
    elected: 2019
  - name: Adriaan de Groot
    title: Board Member
    email: groot<span>@</span>kde.org
    description: Adriaan encountered KDE in 1999 and has been involved in various roles
      ever since. He served on the board of KDE e.V. 2006-2011, and participated in KDE PIM
      development, FreeBSD porting, Solaris porting, the creation of the English Breakfast
      Network (for code-quality checking) and api.kde.org. He can usually be found in the
      Netherlands and works for Blue Systems as the maintainer of Calamares, the independent
      Linux System Installer as well as doing contract work for Free Software projects.
    image: /corporate/pictures/adriaan.jpg
    elected: 2019
---

KDE e.V. elects a board of directors, which represents KDE e.V.
and runs its business.

You can contact the board of KDE e.V. at <a
href="mailto:kde-ev-board@kde.org">kde-ev-board@kde.org</a>.

## Board of Directors

{% for people in page.board %}
<div class="d-flex mb-4">
  <div class="mr-3">
    <img src="{{ people.image }}" width="160" height="160" />
  </div>
  <div class="people-content">
    <h3 class="mt-0">{{ people.name }}</h3>
    <p class="people-title">{{ people.title }}</p>
    <p class="people-email">{{ people.email }}</p>
    <p>{{ people.description }}</p>
    <p>(<i>Elected:</i> {{ people.elected }})</p>
  </div>
</div>
{% endfor %}

## Previous Boards

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
  google.charts.load("current", {packages:["timeline"]});
  google.charts.setOnLoadCallback(drawChart);
  /* JS dates have a 0-based month, keep the data readable */
  function dd(y, m, d) { return new Date(y, m-1, d); }
  /* Swap columns so we can write an aligned table in the data */
  function sc(title, start, end, name) { return [title, name, start, end]; }
  function drawChart() {
    var container = document.getElementById('boardtimeline');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();

    dataTable.addColumn({ type: 'string', id: 'Term' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });

    var CURRENT = dd(2022, 09, 16);
    var president = 'President'
        , treasurer = 'Vice President & Treasurer'
        , vp = 'Vice President'
        , boardA = 'Board Member A'
        , boardB = 'Board Member B'
    ;
    dataTable.addRows([
      sc( president,  dd(1997, 11, 26), dd(1999, 10, 09), 'Matthias Ettrich' ),
      sc( president,  dd(1999, 10, 09), dd(2002, 08, 25), 'Kurt Granroth' ),
      sc( president,  dd(2002, 08, 25), dd(2005, 08, 26), 'Kalle Dalheimer' ),
      sc( president,  dd(2005, 08, 26), dd(2007, 11, 04), 'Eva Brucherseifer' ),
      sc( president,  dd(2007, 11, 04), dd(2009, 07, 07), 'Aaron Seigo' ),
      sc( president,  dd(2009, 07, 07), dd(2014, 08, 22), 'Cornelius Schumacher' ),
      sc( president,  dd(2014, 08, 22), dd(2019, 09, 09), 'Lydia Pintscher' ),
      sc( president,  dd(2019, 09, 09), CURRENT         , 'Aleix Pol i Gonzàlez' ),
      sc( treasurer,  dd(1997, 11, 25), dd(1999, 10, 09), 'Kalle Dalheimer' ),
      sc( treasurer,  dd(1999, 10, 09), dd(2005, 08, 26), 'Mirko Böhm' ),
      sc( treasurer,  dd(2005, 08, 26), dd(2009, 07, 07), 'Cornelius Schumacher' ),
      sc( treasurer,  dd(2009, 07, 07), dd(2012, 07, 03), 'Frank Karlitschek' ),
      sc( treasurer,  dd(2012, 07, 03), dd(2014, 08, 22), 'Agustín Benito Bethencourt' ),
      sc( treasurer,  dd(2014, 08, 22), dd(2017, 07, 22), 'Marta Rybczynska' ),
      sc( treasurer,  dd(2017, 07, 22), CURRENT         , 'Eike Hein' ),
      sc( vp,         dd(1997, 11, 25), dd(1999, 10, 09), 'Martin Konold' ),
      sc( vp,         dd(1999, 10, 09), dd(2002, 08, 25), 'Chris Schläger' ),
      sc( vp,         dd(2002, 08, 25), dd(2005, 08, 26), 'Eva Brucherseifer' ),
      sc( vp,         dd(2005, 08, 26), dd(2006, 09, 25), 'Mirko Böhm' ),
      sc( vp,         dd(2006, 09, 25), dd(2011, 08, 09), 'Adriaan de Groot' ),
      sc( vp,         dd(2011, 08, 09), dd(2013, 07, 12), 'Sebastian Kügler' ),
      sc( vp,         dd(2013, 07, 12), dd(2014, 08, 22), 'Lydia Pintscher' ),
      sc( vp,         dd(2014, 08, 22), dd(2019, 09, 09), 'Aleix Pol i Gonzàlez' ),
      sc( vp,         dd(2019, 09, 09), CURRENT         , 'Lydia Pintscher' ),
      sc( boardA,     dd(1997, 11, 25), dd(1999, 10, 09), 'Michael Renner' ),
      sc( boardA,     dd(1999, 10, 09), dd(2002, 08, 25), 'Preston Brown' ),
      sc( boardA,     dd(2002, 08, 25), dd(2004, 08, 20), 'Marie Loise Nolden' ),
      sc( boardA,     dd(2004, 08, 20), dd(2005, 08, 26), 'Harry Porten' ),
      sc( boardA,     dd(2005, 08, 26), dd(2007, 11, 04), 'Aaron Seigo' ),
      sc( boardA,     dd(2007, 11, 04), dd(2011, 08, 09), 'Sebastian Kügler' ),
      sc( boardA,     dd(2011, 08, 09), dd(2013, 07, 12), 'Lydia Pintscher' ),
      sc( boardA,     dd(2013, 07, 12), dd(2016, 09, 01), 'Albert Astals Cid' ),
      sc( boardA,     dd(2016, 09, 01), dd(2019, 09, 09), 'Thomas Pfeiffer' ),
      sc( boardA,     dd(2019, 09, 09), CURRENT         , 'Neofytos Kolokotronis' ),
      sc( boardB,     dd(1997, 11, 25), dd(2007, 11, 03), '(added in 2007)' ),
      sc( boardB,     dd(2007, 11, 04), dd(2009, 07, 07), 'Klaas Freitag' ),
      sc( boardB,     dd(2009, 07, 07), dd(2012, 07, 03), 'Celeste Lyn Paul' ),
      sc( boardB,     dd(2012, 07, 03), dd(2015, 07, 24), 'Pradeepto Bhattacharya' ),
      sc( boardB,     dd(2015, 07, 24), dd(2018, 08, 13), 'Sandro Andrade' ),
      sc( boardB,     dd(2018, 08, 13), dd(2019, 09, 09), 'Andy Betts' ),
      sc( boardB,     dd(2019, 09, 09), CURRENT         , 'Adriaan de Groot' ),
    ]);

    var options = {
        width: 1000
    };
    chart.draw(dataTable, options);
  }
</script>

<div id="boardtimeline" style="height: 400px; width: 90%"></div>
