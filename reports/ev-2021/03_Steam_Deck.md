<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Products/SteamDeck02.png" alt="Staam Deck game console" />
    </figure>
</div>

In November, Valve announced the [Steam Deck](https://www.steamdeck.com/) – a handheld gaming device running KDE Plasma under the hood! By using a Linux-based OS, Valve is hugely improving the gaming space on Linux. And by running KDE Plasma, tons of people will gain exposure to KDE's software when they use the device docked with a monitor, keyboard, and mouse, because yes, you can do that! This thing is a real computer and can be used like one too!
