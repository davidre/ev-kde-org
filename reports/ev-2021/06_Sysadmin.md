2021 was a year of continued building on foundations laid down in prior years, with much being achieved.

Gitlab continued to be an area of focus for us, with our Continuous Integration system being adapted to work natively. This involved a complete rewrite of the tooling due to significant differences between how Jenkins and Gitlab CI operate, and also allowed us to offer CI services both as part of code review as well as on developers working branches for the first time. These changes also greatly simplified the addition of Qt 6 builds. Additionally to this, we also rewrote a portion of the hooks which process all Git commits, allowing us to introduce functionality available in newer versions of Git.

Continuous Delivery services were also improved during the year, with a stable release F-Droid repository being established for Android application builds. Significant progress was also made surrounding our API Documentation, which to date has been generated on a single server whose setup is difficult to replicate. During the year progress was made on transitioning both to newer tooling which is more maintainable, as well as to a Docker image which can be run by anyone on a local system. We expect the transition to this new setup to be completed in 2022, which will improve the long term maintainability and sustainability of our infrastructure.

The replacement of older parts of our infrastructure also continued, with the servers for KDE Identity and our translation services both being replaced. These changes have improved the security of both, and in the case of translation services also expanded our capacity to complete nightly processing tasks in a more timely manner.

* Created 42 subversion accounts
* Disabled 3 subversion accounts

* Created 3 [kdemail.net](http://kdemail.net/) aliases
* Disabled 1 [kdemail.net](http://kdemail.net/) aliases

* Created 7 [kde.org](http://kde.org/) aliases
* Disabled 1 [kde.org](http://kde.org/) aliases
* Modified 4 [kde.org](http://kde.org/) aliases

* Created 11 [kde.org](http://kde.org/) mailing-lists: kde-l10n-my kstopmotion energy-efficiency kgeotag kde-ev-free-qt-wg kde-l10n-ta kde-l10n-ar kde-l10n-fi kde-i18n-sq kphotoalbum
* Disabled 1 [kde.org](http://kde.org/) mailing-lists: kde-ev-patrons
