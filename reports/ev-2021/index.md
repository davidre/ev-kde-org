---

title: "2021 KDE e.V. Report summary: KDE e.V. latest report is out. In this issue, we cover the activities of our association in 2021. KDE's yearly report gives a comprehensive overview of all that has happened during 2021. It covers the progress we have made with KDE's Plasma and applications as well as results from community sprints, conferences, and external events the KDE community has participated in worldwide."

layout: report

date: 2022-04-30
year: 2021
issue_number: 38

menu:
    - title: Home
      anchor: home
      home: true
    - title: Welcome
      heading: Welcome to KDE's Annual Report 2021
      image: images/logos/kde-logo.png
      author: Aleix Pol
      include: 00_welcome.md
    - title: Featured Article
      heading: Featured article - The Year KDE Turned 25
      image: images/logos/kde-logo.png
      author: Paul Brown
      include: 00_Featured.md
    - title: Supported Activities
      items:
        - title: Developer Sprints and conferences
          heading: Supported Activities ‒ Developer Sprints and Conferences
          items:
# Events ordered according to the date they were held
            - include: 01_PIM_Sprint.md
              author: By the KDE PIM Team
              heading: PIM Sprint
              image: images/logos/kde-pim-logo.png
            - include: 01_Plasma_Mobile_Sound_Contest.md
              author: By the Plasma Mobile & LMMS Teams
              heading: Plasma Mobile/LMMS Sounds Contest
              image: images/logos/lmms.png
            - include: 01_Wayland_Sprint.md
              author: By the Wayland Team
              heading: Wayland Goal Sprint
              image: images/logos/wayland.png
            - include: 01_SoK.md
              author: By the SoK Team
              heading: Season of KDE
              image: images/logos/kde-logo.png
            - include: 01_KF6_Sprint.md
              author: By the Frameworks Team
              heading: Frameworks 6 Virtual Sprint
              image: images/logos/kde-logo.png
            - include: 01_LAS.md
              author: By Aniqa Khokhar
              heading: Linux App Summit
              image: images/logos/LAS.png
            - include: 01_Meet_Sprint.md
              author: By the Meet Team
              heading: Meet Mini Sprint
              image: images/logos/kde-logo.png
            - include: 01_Akademy.md
              author: By Paul Brown
              heading: Akademy
              image: images/logos/akademy_logo.png
            - include: 01_Fundraising_Sprint.md
              author: By the Fundraising Team
              heading: Online Fundraising Sprint
              image: images/logos/kde_logo.png
            - include: 01_NeoChat_Sprint.md
              author: By the Neochat Team
              heading: NeoChat Mini Sprint
              image: images/logos/neochat.png
            - include: 01_Akademy-es.md
              author: By Aniqa Khokhar
              heading: Akademy-es
              image: images/logos/akademy_logo.png
            - include: 01_Eco_Sprint.md
              author: By Joseph P. De Veaugh-Geiss
              heading: KDE Eco Sprint
              image: images/logos/KDE-eco-logo.png
        - title: Projects
          heading: Projects & Apps
          items:
## Items ordered alphabetically
            - include: 02_Angelfish.md
              author: By the Angelfish Team
              heading: Angelfish - A Light Web Browser for your Phone
              image: images/logos/angelfish.png
            - include: 02_Haruna.md
              author: By the Haruna Team
              heading: Haruna - Play Online Videos
              image: images/logos/haruna.png
            - include: 02_Kalendar.md
              author: By the Kalendar Team
              heading: Kalendar - Modern Calendar and Task Manager
              image: images/logos/kalendar.png
            - include: 02_Kalk.md
              author: By the Kalk Team
              heading: Kalk - A Calculator for your Phone and Desktop
              image: images/logos/kalk.png
            - include: 02_Kgeotag.md
              author: By the KGeoTag Team
              heading: KGeoTag - Place and Locate your Pics on a Map
              image: images/logos/kgeotag.png
            - include: 02_Koko.md
              author: By the Koko Team
              heading: Koko - Convergent Image Viewer
              image: images/logos/koko48.png
            - include: 02_Plasma_Mobile_Phonebook.md
              author: By the Phonebook Team
              heading: Phonebook - Manage Contacts on your Desktop and Phone
              image: images/logos/phonebook.png
            - include: 02_Skanpage.md
              author: By the Skanpage Team
              heading: Skanpage - Lightweight Scanning Utility
              image: images/logos/skanpage.png
            - include: 02_Subtitle_Composer.md
              author: By the Subtitle Composer Team
              heading: Subtitle Composer - A Full-Featured Subtitling App
              image: images/logos/subtitlecomposer.png
        
        - title: KDE-Powered Products
          heading: Products
          items:
            - include: 03_Pinebook_Pro.md
              author: By the Promo Team
              heading: Pinebook Pro - Everyday Computing with Plasma
              image: images/logos/pine64.png
            - include: 03_Steam_Deck.md
              author: By the Promo Team
              heading: Steam Deck - Valve's Plasma-enabled Gaming Console
              image: images/logos/steam48.png
            - include: 03_Pinephone_Pro.md
              author: By the Promo Team
              heading: PinePhone Pro - 2nd Gen Plasma Mobile Phone
              image: images/logos/pine64.png

    
        - title: Trade Shows and Community Events
          heading: Trade Shows and Community Events
          items:
## Events ordered according to the date they were held
            - include: 04_Fosdem.md
              heading: FOSDEM
              author: By Aniqa Khokhar
              image: images/logos/FOSDEMx47.png
            - include: 04_FOSSAsia.md
              heading: FOSSAsia
              author: By Aniqa Khokhar
              image: images/logos/FOSSAsia.png
            - include: 04_Calamares_Hacktoberfest.md
              heading: Calamares does Hacktoberfest
              author: By Adriaan De Groot
              image: images/logos/hacktoberfest.png
            - include: 04_LGM.md
              heading: Libre Graphics Meeting
              author: By Aniqa Khokhar
              image: images/logos/LGM.png
            - include: 04_GSoC.md
              heading: Google Summer of Code
              author: By Aniqa Khokhar
              image: images/logos/gsoc.png
            - include: 04_Linux_Application_Ecosystem.md
              heading: Linux Application Ecosystem
              author: By Burgess Chang
              image: images/logos/kde-logo.png
            - include: 04_OpenUk.md
              author: By Jonathan Esk-Riddell
              heading: OpenUK Awards
              image: images/logos/openuk.png

    - title: Reports
      items:
        - title: Working Groups
          heading: Working Groups
          items:
            - heading: Sysadmin
              include: 06_Sysadmin.md
              image: images/logos/kde-logo.png
              author: By Ben Cooksley
            - heading: Financial Working Group
              include: 06_FIWG.md
              image: images/logos/kde-logo.png
              author: By Eike Hein, Marta Rybczynska and Till Adam

    - title: Community Highlights
      heading: Highlights
      include: 07_highlights.md
      author: the Promo Team
    - title: Thoughts from Partners
      heading: Thoughts from Partners
      include: 07_quotes.html
      author: ""
      nonav: true
    - title: New Members
      heading: New Members
      include: 07_new-members.md
      author: ""
      nonav: true
    - title: KDE e.V. Board of Directors
      heading: KDE e.V. Board of Directors
      include: 07_Board.html
      author: ""
      nonav: true
    - title: Partners
      heading: Partners
      include: 07_partners.md
      author: ""
      nonav: true
---

<section id="about-kdeev" class="bg-light">
    <div class="container">
        <h2>About KDE e.V.</h2>
        <p>
            <a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a registered non-profit organization that represents the <a href="http://www.kde.org" target="_blank">KDE Community</a> in legal and financial matters. The KDE e.V.'s purpose is the promotion and distribution of free desktop software in terms of free software, and the program package "K Desktop Environment (KDE)" in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research.
        </p>
        <ul class="address hidden">
            <li><i class="fa fa-map-marker"></i><span> Address:</span> Prinzenstraße 85 F,10969 Berlin, Germany </li>
            <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
            <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
            <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
        </ul>
    </div>
</section> 

<section class="section">
    <div class="container">
        <p>
            Report prepared by Aniqa Khokhar and Paul Brown, with help and sections written by Aleix Pol, Neofytos Kolokotronis, Burgess Chang, Jonathan Esk-Riddell, Adriaan De Groot, Joseph P. De Veaugh-Geiss, the Plasma Team, the Plasma Mobile Team and the Promo Team at large.
        </p>
        
        <p>
            This report is published by KDE e.V., copyright 2021, and licensed under <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons-BY-3.0</a>.
        </p>
        
        <a href="https://ev.kde.org/" target="_blank" class="text-center"> <img class="img-responsive" src="../shared-assets/images/logo.png" alt=""/> </a>
    </div>
</section>
