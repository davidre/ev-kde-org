From the 27th to the 28th of March, the KDE Frameworks team held [a virtual KDE Frameworks 6 Sprint](https://community.kde.org/Sprints/KF6/2021Virtual). One and a half years after the [initial steps towards KF6](https://www.volkerkrause.eu/2019/10/12/kf6-road-to-kde-frameworks6.html), the transition to Qt 6 is getting closer and we needed to map out the next steps forward.

The virtual sprint allowed many more people to participate than we at the [last physical KF6 sprint](https://www.volkerkrause.eu/2019/11/26/kf6-sprint-recap-2019.html), peaking at more than 30 attendees. You can see more details on the [KF6 workboard](https://phabricator.kde.org/project/board/310/) and the [sprint meeting notes](https://share.kde.org/s/BD9B9RDM2SEMK59), but hese are some of the topics that we covered:

* Qt 6 Migration
* ECM and Qt major versions
* Version-less CMake targets
* The structure of the KDE Frameworks
* 5.15 minimum requirement bump timeline.
