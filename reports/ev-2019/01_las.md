<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/LAS/aleix.jpg" width="100%" /><br /><figcaption>Aleix Pol introduces KDE to LAS.</figcaption></figure>
</div>

From the 12th to the 15th of November, the <a href="https://linuxappsummit.org/">Linux App Summit (LAS)</a> was held at La Lleialtat Santsenca cultural center in Barcelona, Spain. It was co-hosted by KDE and Gnome.

LAS intends to impulse all aspects aimed at accelerating the growth of the Linux application ecosystem by bringing together everyone involved in creating a great Linux application user experience. LAS welcomed application developers, designers, product managers, user experience specialists, community leaders, academics, and anyone interested in the state of Linux application design and development.

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/LAS/neil.jpg" width="800" /><br /><figcaption>Neil McGovern speaks about the GNOME project.</figcaption></figure>
</div>

LAS hosted quality sessions all relevant to the subject of the Linux App Ecosystem. Speakers covering the entire spectrum of the Linux desktop came to talk to the conference on topics such as packaging and distribution of apps, monetization within the Linux ecosystem, designing beautiful applications, and more. 

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/LAS/group.jpg" width="800" /><br /><figcaption>LAS is a meeting point of like-minded people.</figcaption></figure>
</div>

The event started with a talk by Mirko Boehm on The Economics of FOSS followed by talks on a wide variety of topics, from independent distribution platforms like Flatpak and Snap, to programming frameworks like Qt and GTK. Speakers included Aleix Pol, Tobias Bernard, Jordan Petridis, Frank Karlitsche, Paul Brown, and many more.  Amit Sagtani and Saloni Garg gave talks on the Open Source culture and the various open-source programs for students and the last day of the conference had many Birds of Feather meetings and hacking sessions.

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/LAS/discussion.jpg" width="800" /><br /><figcaption>Discussing issues affecting Linux applications and strategies that may solve them.</figcaption></figure>
</div>

Compared to 2018, LAS doubled the number of registrations from approximately 80 registrants to 170. The streaming channels peaked at an additional 648 views and the conference was well attended by a 20.2% women and 64.4% men, with the rest preferring not to identify. The event was covered by two journalists and many of our attendees, who wrote blog posts sharing their experiences.

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/LAS/SaloniGarg.jpg" width="800" /><br /><figcaption>Saloni Garg talks of the principles that unify all FOSS projects.</figcaption></figure>
</div>
