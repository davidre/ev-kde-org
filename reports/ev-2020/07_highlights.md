When writing the annual report, we usually ask contributors to the Promo team to suggest what they think the main highlights of the year were. As the Promo team comprises, apart from marketing drones, developers, designers, translators and social media junkies, it offers a varied cross-section of the contributing KDE community and plenty of diverse ideas are thrown around.

However, this year we are doing something different: we are letting the wider audience of followers on social media "pick" what they thought were the most important stories of 2020.

And here they are, the four (one per quarter) most upvoted, re-tweeted, commented, shared and boosted stories we posted to our social media accounts in the year 2020:

### Quarter 1: [Windows 7 versus KDE](https://twitter.com/kdecommunity/status/1214884296596566017)


<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/highlights/Q1.jpg"><img src="images/highlights/Q1.jpg" width="90%" /></a><br /><figcaption>KDE offers a safe haven for Windows 7 refugees.</figcaption></figure>
</div>

In January 2020, support for Windows 7 came to an end. In KDE we strongly believe that the vast majority of users would not miss anything from a closed proprietary system if they moved to an open source environment. So, seeing a window (heh!) of opportunity to attract new users, we whipped up the customized version of Plasma that looked just like Windows 7.

Dominic Hayes, the creator of [Feren OS](https://ferenos.weebly.com/), tweaked his [Seven Black](https://www.pling.com/p/998614) theme and we created a wallpaper that looked similar to that of Windows 7 to make sure refugees felt at home. [We even made a video](https://youtu.be/TJzfaqRLfpY).

Everybody, it seemed, had friends and family who were being forced to "upgrade" to something they didn't want. Moving to a platform that is open and respectful towards its users was a sideways move that resonated with our audience, making our spur-of-the-moment post a hit with our followers.

### Quarter 2: [KNOME](https://twitter.com/kdecommunity/status/1245261102780428288)

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/highlights/Q2.jpg"><img src="images/highlights/Q2.jpg" width="90%" /></a><br /><figcaption>KDE and GNOME join forces.</figcaption></figure>
</div>

Throughout the year, we have several dates in which the Promo team likes to have a bit of fun with users. April 1, April Fools' Day, is one of them.

In 2020, we decided to publish a "joke" that we had been kicking around for at least a year. We bought a domain, set up a website and announced to the world the launch of KNOME, a desktop that combined the best of KDE's Plasma desktop and GNOME's desktop.

We must've touched a nerve, because the net went wild. The post turned into the most popular we have ever published by a long shot. Still today, if you search for "KNOME", literally hundreds of sites will pop up with videos, articles and blog posts commenting on the joke, some humorously, others using KNOME as a metaphor for the state of fragmentation in Open Source projects.

And, indeed, there must be a lesson in there, somewhere, that we can learn from. Is it that there is a genuine desire for one unified desktop for the Linux and BSD's platforms? But we like diversity in FLOSS, right? We like having options. There are different desktops with different philosophies because there are different people that like interacting in different ways with their machines.

The thing is KNOME sparked a discussion... Or added fuel to one that was already ongoing. It helped that our friends at GNOME were also in on the joke and committed to it on their side too.

Which brings us to...

### Q3: [GNOME 3.38 is released](https://twitter.com/kdecommunity/status/1306308257301114883)

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/highlights/Q3.png"><img src="images/highlights/Q3.png" width="90%" /></a><br /><figcaption>Free Software projects stick together.</figcaption></figure>
</div>

Yep, the most popular post on social media for the 3rd quarter was not about a KDE project at all.

Again, an example of how silly the "KDE vs. GNOME" controversy in the face of how both communities enjoy celebrating each other’s successes...


### Q4: [Plasma 5.20 is released](https://twitter.com/kdecommunity/status/1315979526917287937)

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;"><a href="images/highlights/Q4.jpg"><img src="images/highlights/Q4.jpg" width="90%" /></a><br /><figcaption>Plasma 5.20 brought a massive overhaul to the desktop.</figcaption></figure>
</div>

... Because [the same happened in October when we published Plasma 5.20](https://twitter.com/gnome/status/1316047586441142273), when GNOME cheered on KDE's breakthrough release that brought a massive overhaul to dozens of components, widgets, and the desktop behavior in general.

---

The common thread through all these highlights seems to be that our followers get excited by and crave change. They encourage and cheer on people willing to take a leap of faith and move from a closed, proprietary environment to a free and open one, scary as that is; they support innovation and collaboration that lead to progress; and care little about blind loyalty to one single project, preferring to believe that projects with similar aims and principles, even if they have different approaches, are more likely to improve than detract from each other.
