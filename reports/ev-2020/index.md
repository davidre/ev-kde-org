---

title: "2020 KDE e.V. Report summary: KDE e.V. latest report is out. In this issue, we cover the activities of our association in 2020. KDE's yearly report gives a comprehensive overview of all that has happened during 2020. It covers the progress we have made with KDE's Plasma and applications as well as results from community sprints, conferences, and external events the KDE community has participated in worldwide."

layout: report

date: 2021-06-07
year: 2020
issue_number: 37

menu:
    - title: Home
      anchor: home
      home: true
    - title: Welcome
      heading: Welcome to KDE's Annual Report 2020
      image: images/logos/kde-logo.png
      author: Aleix Pol
      include: 00_welcome-message.md
    - title: Featured Article
      heading: Featured article - KDE in Times of COVID
      image: images/logos/kde-logo.png
      author: Paul Brown
      include: 00_featured-article.md
    - title: Supported Activities
      items:
        - title: Developer Sprints and conferences
          heading: Supported Activities ‒ Developer Sprints and Conferences
          items:
# Events ordered according to the date they were held
            - include: 01_SoK.md
              author: By the SoK Team
              heading: Season of KDE
              image: images/logos/kde-logo.png
            - include: 01_conf.kde.in.md
              author: By Adriaan de Groot, Anupam Basak, Sashmita Raghav and Subin Siby
              heading: conf.kde.in
              image: images/logos/logo-kde-india.png
            - include: 01_Plasma_Mobile_sprint.md
              author: By the Plasma Mobile Team 
              heading: Plasma Mobile Sprint
              image: images/logos/kdeconnect.png
            - include: 01_PIMSprint.md
              author: By Daniel Vrátil and Volker Krause
              heading: PIM Sprint & Beyond
              image: images/logos/kde-pim-logo.png
            - include: 01_PlasmaSprint.md
              author: By The Plasma team
              heading: Plasma Sprint
              image: images/logos/plasma.png
            - include: 01_Akademy.md
              author: By Blumen Herzenschein, David C. and Paul Brown
              heading: Akademy
              image: images/logos/akademy_logo.png
            - include: 01_QtCon_Brasil.md
              author: Compiled from several sources 
              heading: QtCon Brasil
              image: images/logos/qtconbr.png
            - include: 01_LAS.md
              author: By Allyson Alexandrou, Aleix Pol, Neofytos Kolokotronis and Kenny Coyle
              heading: Linux App Summit
              image: images/logos/LAS.png
            - include: 01_Akademy-es.md          
              author: By Baltasar Ortega
              heading: Akademy-es
              image: images/logos/akademy_logo.png
            - include: 01_LaKademy.md
              author: By Baltasar Ortega
              heading: LaKademy
              image: images/logos/lakademy2020.png

        - title: Projects
          heading: Projects
          items:
# Items ordered according to the date of release
            - include: 02_Ikona.md
              author: Compiled from the blog of Jan Pontaoski
              heading: Ikona - Create Cool Icons
              image: images/logos/ikona.png
            - include: 02_Kid3.md
              author: By the Kid3 Team
              heading: Kid3 - A Simple, but Powerful Music Tagger
              image: images/logos/kid3.png
            - include: 02_Kup.md
              author: By the Kup Team
              heading: Kup - Make Backups Simple
              image: images/logos/kup.png
            - include: 02_Kontrast.md
              author: By Carl Schwan
              heading: Kontrast - Pick the Perfect Colors
              image: images/logos/kontrast.png            
            - include: 02_Calindori.md          
              author: By the Calindori Team
              heading: Calindori - Touch-Enabled Calendar
              image: images/logos/calindori.png
            - include: 02_System_Monitor.md
              author: By Arjen Hiemstra
              heading: System Monitor - What is you Computer Doing?
              image: images/logos/sysmon.png
            - include: 02_Peruse.md
              author: By Leinir
              heading: Peruse Creator - Make Your Own Comic Books
              image: images/logos/peruse.png
            - include: 02_Spacebar.md
              author: By Bhushan Shah
              heading: Spacebar - Mobile SMS Chat App
              image: images/logos/spacebar.png
        
        - title: Products
          heading: Products
          items:
            - include: 03_Slimbook.md
              author: By Paul Brown
              heading: KDE Slimbook III
              image: images/logos/kde-logo.png
            - include: 03_PinePhone_KCE.md
              author: By Paul Brown
              heading: PinePhone - KDE Community Edition
              image: images/logos/pine64.png
 
    
        - title: Trade Shows and Community Events
          heading: Trade Shows and Community Events
          items:
# Events ordered according to the date they were held
            - include: 04_fosdem.md
              heading: FOSDEM
              author: Compiled from the blogs of Jonathan Riddel, Marco Martin, Volker Krause and others
              image: images/logos/FOSDEMx47.png
            - include: 04_HackIllinois.md
              heading: HackIllinois
              author: By Nate Graham
              image: images/logos/hackillinois.png
            - include: 04_LGM.md
              heading: Libre Graphics Meeting
              author: Compiled from the blog of Timothée Giet
              image: images/logos/LGM.png
            - include: 04_GNUHealthCon.md
              author: By Luis Falcón
              heading: GNUHealthCon
              image: images/logos/gnuhealthcon.png

    - title: Reports
      items:
        - title: Working Groups
          heading: Working Groups
          items:
            - heading: Sysadmin
              include: 06_sysadmin.md
              image: images/logos/kde-logo.png
              author: By Ben Cooksley
            - heading: Financial Working Group
              include: 06_FIWG.md
              image: images/logos/kde-logo.png
              author: By Eike Hein, Neofytos Kolokotronis and David Edmundson

    - title: Community Highlights
      heading: Highlights
      include: 07_highlights.md
      author: the Promo Team
    - title: Thoughts from Partners
      heading: Thoughts from Partners
      include: 07_quotes.html
      author: ""
      nonav: true
    - title: New Members
      heading: New Members
      include: 07_new-members.md
      author: ""
      nonav: true
    - title: KDE e.V. Board of Directors
      heading: KDE e.V. Board of Directors
      include: 07_board.html
      author: ""
      nonav: true
    - title: Partners
      heading: Partners
      include: 07_partners.md
      author: ""
      nonav: true
---

<section id="about-kdeev" class="bg-light">
    <div class="container">
        <h2>About KDE e.V.</h2>
        <p>
            <a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a registered non-profit organization that represents the <a href="http://www.kde.org" target="_blank">KDE Community</a> in legal and financial matters. The KDE e.V.'s purpose is the promotion and distribution of free desktop software in terms of free software, and the program package "K Desktop Environment (KDE)" in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research.
        </p>
        <ul class="address hidden">
            <li><i class="fa fa-map-marker"></i><span> Address:</span> Prinzenstraße 85 F,10969 Berlin, Germany </li>
            <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
            <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
            <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
        </ul>
    </div>
</section> 

<section class="section">
    <div class="container">
        <p>
            Report prepared by Paul Brown and Aniqa Khokhar, with help and contributions from Carl Schwan, Aleix Pol, Neofytos Kolokotronis, Adriaan de Groot, Anupam Basak, Sashmita Raghav, Subin SibyBen Cooksley, Bhushan Shah, Niccolò Venerandi, David Edmundson, Jonathan Riddell, Kai Uwe, Daniel Vrátil,  Volker Krause, Blumen Herzenschein, David C., Allyson Alexandrou, Kenny Coyle, Nate Graham, Baltasar Ortega, Jan Pontaoski, Arjen Hiemstra, Leinir, Marco Martin, Timothée Giet, Luis Falcón, the Plasma Team, the Plasma Mobile Team and the Promo Team at large.
        </p>
        
        <p>
            This report is published by KDE e.V., copyright 2021, and licensed under <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons-BY-3.0</a>.
        </p>
        
        <a href="https://ev.kde.org/" target="_blank" class="text-center"> <img class="img-responsive" src="../shared-assets/images/logo.png" alt=""/> </a>
    </div>
</section>
