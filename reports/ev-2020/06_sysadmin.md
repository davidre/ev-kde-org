2020 was a year of substantial change for the Sysadmin team, with a number of significant systems changing this year.

The most significant of these was the [migration to Gitlab](https://gitlab.com/gitlab-org/gitlab/-/issues/24900), with code hosting and review being successfully transitioned from our previous Gitolite and Phabricator setup. This involved not only importing the repositories, but also developing custom tooling where needed. This custom tooling supports functionality including our custom commit keywords, syncing of user account information from [KDE Identity](<https://identity.kde.org/>) and the automated updating of project details on Gitlab.

Another significant achievement was the deployment of [BigBlueButton](https://bigbluebutton.org/), an online video conferencing system. While initially trialled to allow for the first remote [Akademy](https://akademy.kde.org/2021), it has gone on to be used extensively for remote sprints as well as other community meetings to date. In addition to this, we also replaced our previous mirror management system (Mirrorbrain) with a new setup based on Mirrorbits. This change brought support for HTTPS mirrors, full support for IPv6 and improved the accuracy of matching people with the most appropriate mirror in addition to reducing the load on the server. At the same time, we also deployed a Tirex instance for rendering maps which are used in [Marble](https://marble.kde.org/) and [KItinerary](https://api.kde.org/kdepim/kitinerary/html/index.html). This has allowed us to offer both higher resolution and more up to date maps, improving the experience for those users.

During the year, we also rolled out a new service, [ActivityFilter](https://invent.kde.org/sysadmin/activityfilter). This replaces the Commitfilter service that was discontinued several years ago and also expands its coverage to include Bugzilla, as well as Gitlab, merge requests and tasks, allowing members of the community to easily track areas they're specifically interested in. The introduction of [MyKDE](https://my.kde.org/login), a new service to centrally manage user information, was also a significant milestone and also marked the beginning of the process of replacing KDE Identity.

Going forward, we expect 2021 to be a year of consolidation, with Continuous Integration and Tasks both migrating to Gitlab and more services being transitioned to use MyKDE.

---

Created 73 subversion accounts

Disabled 4 subversion accounts

---

Created 5 kdemail.net aliases

---

Created 24 kde.org aliases

Disabled 9 kde.org aliases

Modified 12 kde.org aliases

---

Created 6 kde.org mailing-lists: neon-commits listowners bugsquad kde-i18n-cs rolisteam kde-l10n-hi

Disabled 65 kde.org mailing-lists: kgraphviewer-devel wikitolearn-core wikitolearn-promo koffice www-pl koffice-devel kde-kamoso kde-ev-hiring kde-ev-marketing social-desktop khtml-devel kde-i18n-nds kde-hardware-devel bangarang kde-networkmanager freenx-knx kde-apps-org campkde-participants kde-pim-meeting kde-events kontact wikitolearn-tech api-comments amarok-promo kairo-devel kdeev-books open-collaboration-services kde-ux-meeting kde-metrics parley-devel kde-enterprise-web kde-oldies w2l-editors kde-connect-team funq-devel kde-events-au kde-usability-devel akademy-br w2l-editors-it noatun-bugs kde-bretzn kexi-pl raptor kde-notes-announce kde-teaching kde-usability kde-events-fr kde-hci antispam-taskforce grancanaria amarok-committee kde-graphics-devel atelier kde-services-devel kde-i18n-vi kde-press-announce-pl kde-debian-private kde-cl kde-testing kde-distro-packagers kde-sonnet kde-openserver amarok-private gluon kde-kiosk