---
title: "KDE's flagship conference Akademy Concludes: Pushing for Elegance and the Mobile Space"
date: 2010-07-16 00:00:00 
layout: post
---

<p>
			Tampere, Finland, 16th July, 2010. KDE met for its yearly flagship conference, Akademy, in Tampere, Finland. The event was kindly hosted by COSS, the Finnish Centre for Open Source Solutions. Akademy started last weekend with a two-day conference attended by more than 400 visitors from all over the world, which then blended into several days of designing, programming, discussing and working on the future of the Free Desktops. Important topics included mobile devices, community topics and many others.
			</p>
        Read more in the <a href="http://ev.kde.org/announcements/2010-07-16-akademy2010-concludes.php">full press release</a>.
      
