---
title: 'KDE e.V. is looking for an event organiser'
date: 2021-12-21 14:30:00
layout: post
noquote: true
---

> Edit 2022-02-16: applications for this position **are closed**.


KDE e.V., the non-profit organisation supporting the KDE community, is looking for someone to help organise KDE's event, including our annual global conference Akademy.
