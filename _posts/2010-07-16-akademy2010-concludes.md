---
title: "KDE's flagship conference Akademy Concludes: Pushing for Elegance and the Mobile Space"
date: 2010-07-16 00:00:00 
layout: post
---

Tampere, Finland, 16th July, 2010. KDE met for its yearly flagship conference, Akademy, in Tampere, Finland. The event was kindly hosted by COSS, the Finnish Centre for Open Source Solutions. Akademy started last weekend with a two-day conference attended by more than 400 visitors from all over the world, which then blended into several days of designing, programming, discussing and working on the future of the Free Desktops. Important topics included mobile devices, community topics and many others. 

Ilkka Lehtinen (COSS) concludes: "<em>COSS is happy to give back to the Free Desktop community by way of organizing their flagship conference. It was great to see the productivity and energy the KDE community is working on a wide range of topics. We are happy to once again show that Finland, and especially Tampere is one of the hubs of Free Software. Having the KDE community as a guest in Tampere was a nice experience, and while such a big and long conference can be a bit taxing at times, it was absolutely worth the effort.</em>", Sebastian Kügler, board member of the KDE e.V. notes: "<em>It's amazing to see the KDE community growing, both in size of the community as well as in scope. KDE has a lot of highly interesting software to provide, especially for Linux-based operating systems for mobile devices. During this Akademy, we made clear that KDE is more than ready to take on the mobile space. We're very thankul to COSS for making this event possible, COSS has proven to be a great contributor to KDE this way.</em>"

KDE's chief motivator Aaron Seigo proposed a common direction for KDE development to take: <strong>Elegance</strong>. The concept of elegance combines intuitive and beautiful user interfaces with technologically outstanding solutions. In the consecutive hacking sessions, many developers started thinking how they can make their software more elegant, and thus much more attractive to use.

Using KDE software on <strong>mobile devices</strong> was another big subject of discussion and coding. The KDE community is very interested in providing their software for mobile platforms such as MeeGo. During this Akademy, work continued on making Akonadi (and in extension the Kontact Groupware Suite) and the Plasma user interface library available on MeeGo. Like the KDE community, MeeGo aims to support a full spectrum of devices in terms of formfactor and performance. <strong>Kontact Mobile</strong> provides the most scalable and powerful groupware client currently available for mobile devices, while the Plasma universal canvas provides the most mature high-level, extensible and brandable toolkit for mobile devices that are using Qt. During Akademy, the first phone call using prototype <strong>Plasma mobile phone</strong> shell was made.

<strong>Local communities</strong> played another big role in this year's Akademy. During the past years, KDE has seen a tremendous growth in the communities in especially India and Brazil. This year, Akademy saw the largest contingent from these strong Free software countries ever. Many people from India and Brazil have joined this year's Akademy to connect with the international KDE community and to become ambassadors in their home countries for the Free desktop and KDE software.

Akademy was co-organized by COSS, the Finnish Center for Open Source Solutions and KDE e.V., the foundation backing the KDE community organizationally, legally and financially.

<h2>About KDE</h2>

The KDE® Community is an international technology team dedicated to creating a free and user-friendly computing experience, offering an advanced graphical desktop, a wide variety of applications for communication, work, education and entertainment and a platform to easily build new applications upon. We have a strong focus on finding innovative solutions to old and new problems, creating a vibrant atmosphere open for experimentation. KDE e.V. is the foundation behind the KDE community, providing stewardship in organizational, financial and legal areas.

<h2>About COSS</h2>
<p>
COSS (the Finnish Centre for Open Source Solutions), founded in 2003, is a national development agency for open source business ecosystem in Finland and number one gateway to Finnish open source. COSS promotes the development and adoption of managed and sustainable open source solutions in various industries and public sector.
</p>
