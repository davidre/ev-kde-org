---
title: 'KDE e.V. is looking for a project coordinator'
date: 2019-11-18 00:00:00 
layout: post
noquote: true
---

> Applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a project coordinator who can help with project coordination for KDE's goals initiative and various other projects for the KDE community. Please see the <a href="https://ev.kde.org/resources/projectcoordinator-callforproposals.pdf">call for proposals</a> for more details about this contract opportunity. We are looking forward to your application.
