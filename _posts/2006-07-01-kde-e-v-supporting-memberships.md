---
title: 'KDE e.V. Supporting Memberships'
date: 2006-07-01 00:00:00 
layout: post
---

The rules of procedure for the supporting membership of the KDE e.V.
are <a href="/corporate/supporting_members/">available in English and German</a>.
