---
title: 'KDE e.V. seeking Intern'
date: 2009-11-22 00:00:00 
layout: post
---

KDE e.V. is seeking an intern for the KDE e.V. office in Berlin.
If you are looking for an intern position for 3-6 months
in an international non-profit organization and want to
work on events organization, document management and
business operations support, send your application to
the <a href="/corporate/board/">board of KDE e.V.</a>

The position is an internship under regular German conditions
(<i>Angemessene Bezahlung</i>). We are looking for people with some
experience with event management, trade shows, fairs  and
Free Software; someone enthusiastic and independent, with a
good command of the English language. A background or current
study in business or administration would be nice to have.

<a href="http://opendesktop.org/jobs/?id=74843">Job listing
on OpenDesktop.org</a>

