---
title: 'KDE e.V. is looking for an event organiser'
date: 2020-02-13 00:00:00 
layout: post
noquote: true
---

> Edit 2020-03-04: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is 
looking for an event organiser who will support us in organising our 
upcoming conference Akademy for 2020. 
Please see the 
[call for proposals](/resources/eventorganiser-callforproposals.pdf)
for more details about this contract opportunity. 
We are looking forward to your application.
