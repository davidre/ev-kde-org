---
title: "General Assembly"
layout: page
---

> The *General Assembly* or *Annual General Meeting* (AGM) is the yearly meeting where members of the association come together to discuss matters related to the organisation and perform their duties such as electing board members and discharge the existing board.

## General Assembly 2022

The next general assembly of KDE e.V. takes place at the Vertex building at Universitat Politècnica de Catalunya in Barcelona, Spain
on Monday 3rd of October at 10am.

All members of KDE e.V. are invited to attend the general assembly.

See the [agenda](agenda) for what is planned to happen at the general assembly.

If you are not able to participate in the general assembly in person you can,
according to section 6.6 of the articles of association of the KDE e.V., ask
another member to act as proxy for you and execute your voting right. Fill out
the [proxy form](../resources/proxy_instructions.pdf), sign it and make sure that it's available to the chairperson
of the general assembly on the day of the assembly if you make use of this
option. Each active member can act as proxy for up to two other active
members.

Please print out the proxy form, fill it in then scan it. This should then be emailed to your proxy and the <a href="mailto:kde-ev-board@kde.org">board</a>.

If you aren't executing your voting rights personally or through a proxy for
two consecutive general assemblies your membership status will be changed from
active to passive, according to section 4.2 of the articles of association.
Passive and supporting members have the right to attend the general assembly,
but they have no voting rights.

The general assembly will be held at [Akademy](https://akademy.kde.org). Don't miss the opportunity to be part of this exciting event.


## Protocol of the AGM

The official notes of the AGM are taken by the AGM-note-taker and
deposited (in German) with KDE e.V. and the authorities.
Some years have English translations. You can find the
notes of past AGMs on the [reports page](/reports/#meetings).
