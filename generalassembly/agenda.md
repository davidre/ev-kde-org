---
title: "Agenda General Assembly KDE e.V. 2022"
layout: page
---

This is the agenda for the Annual General Assembly (AGM) of KDE e.V. for 2022.
It takes place at the Vertex building at Universitat Politècnica de Catalunya in Barcelona, Spain on Monday 3rd of October at 10am.


## Agenda

1. Welcome
2. Election of a chairman for the general assembly
3. Report of the board
   1. Report about activities
   2. Report of the treasurer
   3. Report of the auditors of accounting
   4. Relief of the board
4. Report of representatives, working groups and task forces of KDE e.V.
   1. Report of the representatives to the KDE Free Qt Foundation
   2. Report of the KDE Free Qt Working Group
   3. Report of the System Administration Working Group
   4. Report of the Community Working Group
   5. Report of the Financial Working Group
   6. Report of the Advisory Board Working Group
   7. Report of the Fundraising Working Group
5. Election of the board 
6. Election of the Auditors of Accounting
7. Election of the Representatives to the KDE Free Qt Foundation
8. Miscellaneous
