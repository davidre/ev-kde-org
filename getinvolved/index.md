---
title: "Get Involved"
layout: page
---

If you are a member of the KDE community who has contributed to promoting, 
developing or documenting KDE or doing any other KDE related work, you can 
join the KDE e.V. as an 
[active member](members/).
People who join the association do so according to the 
[statutes of KDE e.V.](/corporate/statutes/#4).
See the [instructions](members/) for more details on how to join.

If you are a corporation and want to support KDE you can become a 
[supporting member](supporting-members/).

## Donations

**One-time** For a one-time donation, 
the [donations page](https://kde.org/community/donations/)
can be used for any amount through a wide range of payment options.

**Recurring**
If you want to support KDE by regularly donating money, consider becoming a 
[Supporting 
Member](https://relate.kde.org/). The [donations page](https://kde.org/community/donations/)
has additional information on your options.

> KDE e.V. does not accept donations of "crypto" coins: Bitcoin, Etherium,
> Litecoin, Dogecoin and all the others. The tax regime that covers
> German not-for-profit associations considers those coins a form
> of speculation which would endanger our status -- so regretfully
> you cannot donate them directly. Cash out and then use a traditional
> method to donate.
